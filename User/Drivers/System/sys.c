#include "sys.h"


/******************************************************************

外部声明变量


******************************************************************/
extern uint8_t retSD; /* Return value for SD */
extern char SDPath[4]; /* SD logical drive path */
extern FATFS SDFatFS; /* File system object for SD logical drive */
extern FIL SDFile; /* File object for SD */
extern uint8_t retUSER; /* Return value for USER */
extern char USERPath[4]; /* USER logical drive path */
extern FATFS USERFatFS; /* File system object for USER logical drive */
extern FIL USERFile; /* File object for USER */


/******************************************************************

外部声明任务


******************************************************************/
/******************************************************************

全局变量


******************************************************************/
DriversStatus_t drivers_status;
/******************************************************************

系统初始化


******************************************************************/
void sys_Init(void)
{
	FRESULT res = 0;
	BYTE work[_MAX_SS];
	//
	//初始化板载外设
	//	
	W25Q_SPI_Init();
	LY68L_SPI_Init();
	drivers_status.psram = (DriversStatus)LY68L_SPI_Test();
	//
	//初始化文件系统
	//
  retSD = FATFS_LinkDriver(&SD_Driver, SDPath);
  retUSER = FATFS_LinkDriver(&USER_Driver, USERPath);
	//
	//初始化SPI FLASH，绑定文件系统
	//
	res = f_mount(&USERFatFS, (TCHAR const*)USERPath, 1);
	if(res != FR_OK)
	{
		if(res == FR_NO_FILESYSTEM || res == FR_DISK_ERR)
		{
			res = f_mkfs(USERPath, FM_ANY ,0 , work, sizeof work);
		}
		if(res == FR_OK)
		{
			if(f_mount(&USERFatFS, (TCHAR const*)USERPath, 1)==FR_OK)
			{
				drivers_status.spiflash = DRIVERS_OK;
			}
			else
			{
				drivers_status.spiflash = DRIVERS_FAIL;
			}
		}
		else
		{
			drivers_status.spiflash = DRIVERS_FAIL;
		}
	}
	else
	{
		drivers_status.spiflash = DRIVERS_OK;
	}
	//
	//初始化SD卡，绑定文件系统
	//
	res = f_mount(&SDFatFS, (TCHAR const*)SDPath, 1);
	if(res!=FR_OK)
	{
		if(res == FR_NO_FILESYSTEM || res == FR_DISK_ERR)
		{
			res = f_mkfs(SDPath, FM_FAT32 ,0 , work, sizeof work);
		}
		if(res == FR_OK)
		{
			if(f_mount(&SDFatFS, (TCHAR const*)SDPath, 1)==FR_OK)
			{
				drivers_status.sdcard = DRIVERS_OK;
			}
			else
			{
				drivers_status.sdcard = DRIVERS_FAIL;
			}
		}
		else
		{
			drivers_status.sdcard = DRIVERS_FAIL;
		}
	}
	else
	{
		drivers_status.sdcard = DRIVERS_OK;
	}
	//
	//初始化USB
	//
  MX_USB_DEVICE_Init();
	drivers_status.usb = DRIVERS_OK;
}
/******************************************************************
//
//默认任务
//
******************************************************************/
void StartDefaultTask(void const * argument)
{
	sys_Init();
	//
	//APP 启动
	void app_blink_start(void);
	app_blink_start();

	while(1)
	{
		osDelay(10000);
	}

}
/******************************************************************
//
//系统启动
//
******************************************************************/
osThreadId defaultTaskHandle;
void sys_OsStart(void)
{
  osThreadDef(defaultTask, StartDefaultTask, osPriorityIdle, 0, 10240);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);
  osKernelStart();
}